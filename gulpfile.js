var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');

gulp.task('default', function(){
    console.log('Hello friends!');
});


gulp.task('style', function(){
    gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(csso())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('.tmp'))
    .pipe(gulp.dest('dist'))
});

gulp.task('copy', function() {
  gulp.src([
    'src/*',
    '!src/*.html',
	'!src/js/',
	'!src/scss/',
	'!src/img/',
  ], {
    dot: true,
  })
  .pipe(gulp.dest('dist'));
});
gulp.task('default', ['style', 'copy']);